settingsPageAboutDebug: Débogages
settingsPageAboutDebugDialogClose: Ok
errorAppInvalidCurrentBuild: "Erreur : Aucun build trouvé pour la version actuelle"
errorAppCompatibilitySdkVersionTooLow: "Cette application n'est pas compatible avec votre appareil (sdkVersion trop basse : {sdkVersion} < {minSdkVersion})"
errorAppCompatibilityNoMatchingABI: "Cette application n'est pas compatible avec votre appareil (ABI non supportée : {deviceABIs} n'est pas dans {supportedABIs})"
errorAppInstallationShizuku: L'installation de Shizuku a échouée
appPageInstallingShizukuProcess: Installallation de l'app... (Shizuku)
settingsPageShizukuTitle: Service Shizuku 
settingsPageShizukuDescription: Shizuku est une application qui peut donner à SkyDroid l'accès aux API du système sans permission. Ceci permet l'installation et la mise à jour d'applications sans que l'utilisateur n'ait à intervenir. 
settingsPageShizukuToggleSwitch: Activer Shizuku
settingsPageShizukuInstallButton: Installer Shizuku
appPageInstallingShizukuErrorNotRunning: Shizuku Service ne fonctionne pas
appPageInstallingShizukuErrorNotRunningButton: Ouvrir Shizuku
appPageInstallingShizukuErrorPermissionNotGranted: Permission non accordée (Allez dans les réglages de SkyDroid et réactivez le support Shizuku)
removeNamesDialogTitle: Supprimer les noms sélectionnés ? 
removeNamesDialogContent: Voulez-vous vraiment supprimer {count} noms de votre liste d'applciations ? Cette action n'affecte pas vos applications installées et les noms peuvent être ajoutés à nouveau via une collection.
removeNamesDialogConfirm: Tout supprimer
appListPageSearchHint: Rechercher une app...
appListPageUpdateAllAppsButton: Mettre à jour toutes les apps
batchProcessingSheetProgressOne: Traitement de 1 app...
batchProcessingSheetProgressMore: Traitement de {count} apps...
batchProcessingSheetSelectedOne: 1 app sélectionnée
batchProcessingSheetSelectedMore: "{count} apps sélectionnées"
batchProcessingSheetSelectAllButton: Tout sélectionner
batchProcessingSheetUnselectAllButton: Tout déselectionner
batchProcessingSheetShizukuWarning: Activez le service Shizuku dans les réglages pour utiliser les actions groupées
batchProcessingSheetInstallOrUpdateAllButton: Installer / Mettre à jour
batchProcessingSheetUninstallAllButton: Désinstaller
batchProcessingSheetRemoveAllNamesButton: Supprimer tous les noms sélectionnés
